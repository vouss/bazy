1. Wyświetl daty wypożyczenia i zwrotu oraz markę samochodu.

DATA_WYP DATA_ZWR MARKA
-------- -------- ------------
00/01/16 00/02/15 OPEL
00/04/08 00/04/17 FORD
01/09/10 02/01/11 NISSAN
01/12/30 02/01/14 TOYOTA
02/01/10 02/01/29 OPEL
02/11/27 03/02/05 OPEL
03/01/01 03/01/02 OPEL
03/01/02 03/12/30 MERCEDES
03/06/15 04/01/01 FIAT
03/07/01 03/12/01 VOLVO

SELECT DATA_WYP, DATA_ZWR , MARKA FROM SAMOCHODY, WYPORZYCZENIA 
  WHERE SAMOCHODY.ID_SAM = WYPORZYCZENIA.ID_SAM;

// ================================================================
2.Wyświetl koszty wypożyczenia oraz nazwiska klientów.

     KOSZT NAZWISKO
---------- ---------------
      1800 NOWAK
       360 LIPKA
      5535 BRACKI
       675 ADAMCZYK
      1045 NOWAK
      3150 LIPKA
        80 KOWALSKI
     72400 KOWALSKI
      8000 NOWAK
      4590 BRACKI

SELECT KOSZT, NAZWISKO FROM KLIENCI, WYPORZYCZENIA, SAMOCHODY 
  WHERE KLIENCI.ID_KLIENTA = WYPORZYCZENIA.ID_KLI AND WYPORZYCZENIA.ID_SAM = SAMOCHODY.ID_SAM;

// ================================================================
3. Wyświetl nazwiska i imiona wszystkich klientów oraz marki i modele samochodów, jakie wypożyczali.
   Posortuj dane alfabetycznie według nazwisk klientów.

NAZWISKO        IMIE            MARKA        MODEL
--------------- --------------- ------------ ------------
ADAMCZYK        PAWEL           TOYOTA       YARIS
BRACKI          BOGDAN          NISSAN       MICRA
BRACKI          BOGDAN          VOLVO        440
KOWALSKI        PIOTR           OPEL         VECTRA
KOWALSKI        PIOTR           MERCEDES     S600
LIPKA           JAKUB           FORD         K
LIPKA           JAKUB           OPEL         ASTRA
NOWAK           JAN             OPEL         ASTRA
NOWAK           JAN             FIAT         PUNTO
NOWAK           JAN             OPEL         CALIBRA

SELECT NAZWISKO, IMIE, MARKA, MODEL FROM KLIENCI, SAMOCHODY, WYPORZYCZENIA 
  WHERE KLIENCI.ID_KLIENTA = WYPORZYCZENIA.ID_KLI AND
        WYPORZYCZENIA.ID_SAM = SAMOCHODY.ID_SAM ORDER BY KLIENCI.NAZWISKO; 

// ================================================================
4. Wyświetl numery dowodów osobistych, numery rejestracyjne samochodów oraz daty wypożyczeń
   dla wszystkich klientów, którzy wypożyczali samochody zarejestrowane w województwie Krakowskim
  (numery rejestracyjne zaczynające się od KR). Posortuj dane według data wypożyczenia w kolejności odwrotnej.

NR_DOWODU NR_REJ   DATA_WYP
--------- -------- --------
AA1234567 KRC-A120 03/01/02
CE6712098 KRA-1023 02/11/27
CC3478690 KRC-4590 01/09/10

SELECT NR_DOWODU, NR_REJ, DATA_WYP FROM KLIENCI, SAMOCHODY, WYPORZYCZENIA
  WHERE KLIENCI.ID_KLIENTA = WYPORZYCZENIA.ID_KLI AND
        WYPORZYCZENIA.ID_SAM = SAMOCHODY.ID_SAM AND
        SAMOCHODY.NR_REJ LIKE 'KR%'
        ORDER BY WYPORZYCZENIA.DATA_WYP DESC;  

// ================================================================
5. Wyświetl nazwiska, imiona klientów i miejscowości oraz modele samochodów,
 dla samochodów wyprodukowanych w Niemczech i dla klientów zamieszkałych poza Warszawą.

NAZWISKO        IMIE            MIEJSCOWOSC               MODEL
--------------- --------------- ------------------------- ------------
NOWAK           JAN             WROCLAW                   ASTRA
NOWAK           JAN             WROCLAW                   CALIBRA
LIPKA           JAKUB           KATOWICE                  ASTRA

SELECT NAZWISKO, IMIE, MIEJSCOWOSC, MODEL FROM KLIENCI, SAMOCHODY, WYPORZYCZENIA
  WHERE KLIENCI.ID_KLIENTA = WYPORZYCZENIA.ID_KLI AND
        WYPORZYCZENIA.ID_SAM = SAMOCHODY.ID_SAM AND
        SAMOCHODY.KRAJ_PROD = 'NIEMCY' AND
        KLIENCI.MIEJSCOWOSC NOT LIKE 'WARSZAWA';

// ================================================================
6. Wyświetl nazwiska oraz liczbę wypożyczonych samochodów przez poszczególnych klientów.
 Nagłówek kolumny z liczbą wypożyczeń powinien być LICZBA_WYP. 
 Posortuj wynik pod względem wypożyczeń w kolejności od największej do najmniejszej.

NAZWISKO        LICZBA_WYP
--------------- ----------
NOWAK                    3
BRACKI                   2
KOWALSKI                 2
LIPKA                    2
ADAMCZYK                 1

SELECT NAZWISKO, COUNT(NR_REJ) AS LICZBA_WYP FROM KLIENCI, SAMOCHODY, WYPORZYCZENIA
  WHERE KLIENCI.ID_KLIENTA = WYPORZYCZENIA.ID_KLI AND
        WYPORZYCZENIA.ID_SAM = SAMOCHODY.ID_SAM
        GROUP BY KLIENCI.NAZWISKO
        ORDER BY KLIENCI.NAZWISKO DESC;

// ================================================================
7. Wyświetla nazwy miejscowości, z których pochodzą klienci oraz sumy kosztów wypożyczeń 
przez klientów z danej miejscowości. Posortuj otrzymane dane w kolejności od najmniejszego
 do największego kosztu.

MIEJSCOWOSC               SUMA_KOSZTOW
------------------------- ------------
WARSZAWA                         72480
WROCLAW                          10845
GDANSK                           10125
KATOWICE                          3510
POZNAN                             675

SELECT MIEJSCOWOSC, SUM(KOSZT) AS SUMA_KOSZTOW FROM KLIENCI, SAMOCHODY, WYPORZYCZENIA
  WHERE KLIENCI.ID_KLIENTA = WYPORZYCZENIA.ID_KLI AND
        WYPORZYCZENIA.ID_SAM = SAMOCHODY.ID_SAM
        GROUP BY KLIENCI.MIEJSCOWOSC
        ORDER BY SUMA_KOSZTOW DESC;

// ================================================================
8. Wyświetlić nazwiska klientów oraz marki i modele samochodów dla klientów,
 którzy aktualnie maja wypożyczone samochody.

MARKA        MODEL        NAZWISKO
------------ ------------ ---------------
MERCEDES     S600         KOWALSKI
FIAT         PUNTO        NOWAK

SELECT NAZWISKO, MARKA, MODEL FROM KLIENCI, SAMOCHODY, WYPORZYCZENIA
  WHERE KLIENCI.ID_KLIENTA = WYPORZYCZENIA.ID_KLI AND
        WYPORZYCZENIA.ID_SAM = SAMOCHODY.ID_SAM AND
        MONTHS_BETWEEN(TO_DATE('2014\05\02', 'yyYy\mm\dd'), WYPORZYCZENIA.DATA_ZWR) < 0;

// ================================================================
9. Wczytaj do bazy plik Skrypt2.sql. Wyświetlić modele i marki tych samochodów,
 których dzienny koszt wypożyczenia jest niski.

SELECT MODEL, MARKA FROM SAMOCHODY, KOSZTY,WYPORZYCZENIA, KLIENCI
  WHERE KLIENCI.ID_KLIENTA = WYPORZYCZENIA.ID_KLI AND
        WYPORZYCZENIA.ID_SAM = SAMOCHODY.ID_SAM AND
        KOSZTY.WARTOSC LIKE 'NISKI';

// ================================================================
10. Wyświetl numery rejestracyjne tych samochodów, których koszt wypożyczenia nie jest niski ani średni.

SELECT SAMOCHODY.NR_REJ FROM SAMOCHODY, KOSZTY,WYPORZYCZENIA, KLIENCI
  WHERE KLIENCI.ID_KLIENTA = WYPORZYCZENIA.ID_KLI AND
        WYPORZYCZENIA.ID_SAM = SAMOCHODY.ID_SAM AND
        KOSZTY.WARTOSC NOT LIKE 'NISKI' AND
        KOSZTY.WARTOSC NOT LIKE 'SREDNI';  

// ================================================================
11. Wyświetl nazwiska tych klientów, którzy w ciągu ostatnich 6 miesięcy chociaż raz wypożyczyli samochód,
 którego koszt wypożyczenia jest co najmniej wysoki. Wyświetlone mają być również marki i modele samochodów,
  które wypożyczyli oraz całkowity koszt danego wypożyczenia.

  SELECT NAZWISKO, MARKA, MODEL, KOSZT AS CALKOWITY_KOSZT FROM SAMOCHODY, KOSZTY,WYPORZYCZENIA, KLIENCI
  WHERE KLIENCI.ID_KLIENTA = WYPORZYCZENIA.ID_KLI AND
        WYPORZYCZENIA.ID_SAM = SAMOCHODY.ID_SAM AND
        MONTHS_BETWEEN(TO_DATE('2003\02\16', 'yyyy\mm\dd'), WYPORZYCZENIA.DATA_WYP) <= 6 AND
        KOSZTY.WARTOSC LIKE 'WYSOKI' AND
        KOSZTY.WARTOSC LIKE 'BARDZO WYSOKI';

// ================================================================
12. Wyświetl liczbę samochodów dostępnych w wypożyczalni w każdym przedziale cenowym.

Wartosc 			Liczba samochodow 
WYSOKI 		         	1 
NISKI 			         3 
SREDNI 			        5 
BARDZO WYSOKI 	   	1

SELECT COUNT(NR_REJ) AS LICZBA_SAMOCHODOW, KOSZTY.WARTOSC FROM SAMOCHODY, KOSZTY,WYPORZYCZENIA, KLIENCI
  WHERE KLIENCI.ID_KLIENTA = WYPORZYCZENIA.ID_KLI AND
        WYPORZYCZENIA.ID_SAM = SAMOCHODY.ID_SAM GROUP BY KOSZTY.WARTOSC;