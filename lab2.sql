1. Wyświetl marki samochodów oraz modele. Modele mają być wypisane mały literami,
 kolumna ma mieć nie zmienioną nazwę.

MARKA        MODEL
------------ ------------
OPEL         astra
MERCEDES     s600
FORD         k
TOYOTA       yaris
OPEL         vectra
OPEL         astra
VOLVO        440
FIAT         punto
NISSAN       micra
OPEL         calibra

SELECT MARKA, LOWER(MODEL) AS MODEL FROM SAMOCHODY; 

// ================================================================
2. Wyświetl imiona i nazwiska klientów tak, aby zaczynały się z wielkiej litery.
 Nazwy kolumn mają pozostać niezmienione.

IMIE            NAZWISKO
--------------- ---------------
Piotr           Kowalski
Jan             Nowak
Pawel           Adamczyk
Bogdan          Bracki
Jakub           Lipka

SELECT INITCAP(IMIE) AS IMIE, INITCAP(NAZWISKO) AS NAZWISKO FROM KLIENCI;

// ================================================================
3. Wyświetl poniższy raport.

KLIENCI_FIRMY
----------------------------------------
10........PIOTR..........KOWALSKI.......
15........JAN............NOWAK..........
20........PAWEL..........ADAMCZYK.......
25........BOGDAN.........BRACKI.........
30........JAKUB..........LIPKA..........

// ================================================================
4. Wyświetl marki i modele samochodów oraz koszt godzinowy wypożyczenia samochodu
 (nadaj kolumnie nagłówek: koszt_godziny).

MARKA        MODEL        KOSZT_GODZINY
------------ ------------ -------------
OPEL         ASTRA                  1,9
MERCEDES     S600                   8,3
FORD         K                      1,7
TOYOTA       YARIS                  1,9
OPEL         VECTRA                 3,3
OPEL         ASTRA                  2,5
VOLVO        440                    1,3
FIAT         PUNTO                  1,7
NISSAN       MICRA                  1,9
OPEL         CALIBRA                2,3

SELECT MARKA, UPPER(MODEL) AS MODEL, ROUND((KOSZT_DNIA/24),2) AS KOSZT_GODZINY FROM SAMOCHODY; 

 // ================================================================
5. Wyświetl liczbę miesięcy, jakie upłynęły pomiędzy data wypożyczenia a datą obecną.
 Kolumnie nadaj nazwę LICZBA_MIESIECY.

LICZBA_MIESIECY
---------------
             45
             42
             25
             21
             21
             10
              9
              9
              4
              3

SELECT ROUND(MONTHS_BETWEEN(TO_DATE('2014/06/02', 'yyyy/mm/dd'), WYPORZYCZENIA.DATA_WYP)) AS LICZBA_MIESIECY FROM WYPORZYCZENIA; 

// ================================================================
6. Wyświetl minimalny, średni oraz maksymalny koszt dnia dla wszystkich samochodów.
 Nazwy kolumn wynikowych mają być odpowiednio: KOSZT_MINIMALNY, KOSZT_SREDNI, KOSZT_MAKSYMALNY.

KOSZT_MINIMALNY KOSZT_SREDNI KOSZT_MAKSYMALNY
--------------- ------------ ----------------
             30           64              200

SELECT MIN(KOSZT_DNIA) AS KOSZT_MINIMALNY , AVG(KOSZT_DNIA) AS KOSZT_SREDNI, MAX(KOSZT_DNIA) AS KOSZT_MAKSYMALNY FROM SAMOCHODY;

// ================================================================
7. Wyświetl różnicę pomiędzy najmniejszym a największym kosztem dnia.

   ROZNICA
----------
       170

SELECT (MAX(KOSZT_DNIA) - MIN(KOSZT_DNIA)) AS ROZNICA FROM SAMOCHODY;

// ================================================================
8. Wyświetl markę oraz średnią pojemność silnika dla danej marki z tabeli SAMOCHODY.

MARKA         POJEMNOSC
------------ ----------
FIAT                1,4
FORD                1,1
MERCEDES              6
NISSAN                1
OPEL              1,925
TOYOTA              1,2
VOLVO               1,4

SELECT MARKA, AVG(POJ_SIL) AS POJEMNOSC FROM SAMOCHODY GROUP BY MARKA;

// ================================================================
9. Wyświetlić ilość samochodów wyprodukowanych w Niemczech.

SAMOCHODOW_Z_NIEMIEC
--------------------
                   5

SELECT COUNT(NR_REJ) AS SAMOCHODOW_Z_NIEMIEC FROM SAMOCHODY WHERE KRAJ_PROD='NIEMCY';

// ================================================================
10. Wyświetl kraj produkcji, ilość samochodów danego kraju (ilosc) oraz najmniejszy koszt dnia
 (koszt_dnia) dla samochodów z danego kraju.

KRAJ_PROD            ILOSC KOSZT_DNIA
--------------- ---------- ----------
JAPONIA                  2         45
NIEMCY                   5         45
SZWECJA                  1         30
USA                      1         40
WLOCHY                   1         40

SELECT KRAJ_PROD, COUNT(NR_REJ) AS ILOSC, MIN(KOSZT_DNIA) AS KOSZT_DNIA FROM SAMOCHODY GROUP BY KRAJ_PROD;

11. Wyświetl z tabeli WYPOZYCZENIA sumę kosztów wypożyczenia dla wszystkich samochodów,
 które są aktualnie wypożyczone (wykorzystaj funkcję sysdate).

KOSZT_CALKOWITY
---------------
          84990

12. Wyświetl daty najbliższych poniedziałków, przypadających po dacie wypożyczenia dla
 każdego samochodu.

13. Wyświetl daty ostatnich dni miesiąca, przypadających po dacie zwrotu dla każdego
 samochodu.

14. Wyświetl marki wszystkich samochodów. Jeżeli dany samochód jest marki: Opel,
 zamiast tego słowa, powinno zostać wypisane: “General Motors”.

15. Wyświetl całkowity koszt wypożyczenia dla poszczególnych wypozyczeń, uwzględniając
 podatek VAT oraz rabat wyokości 5% od ceny z Vatem.
