-- Kolokwium 3 --
link do zadań
https://mega.co.nz/#!JIE3UYRA!bAdwExnDQNwg-iEf2hbri6vLPb7aySctF4EWXFWAMXw

-- Zadanie 1 --
CREATE TABLE OCENY(
  ID_OCENY NUMBER(2) PRIMARY KEY,
  ID_PRZEDMIOTU NUMBER(2),
  FOREIGN KEY (ID_PRZEDMIOTU) REFERENCES PRZEDMIOTY(ID_PRZEDMIOTU),
  ID_UCZNIA NUMBER(2),
  FOREIGN KEY (ID_UCZNIA) REFERENCES UCZNIOWIE(ID_UCZNIA),
  ID_NAUCZYCIELA NUMBER(2),
  FOREIGN KEY (ID_NAUCZYCIELA) REFERENCES NAUCZYCIELE(ID_UCZNIA),
  OCENA NUMBER(2),
  DATA DATE DEFAULT (sysdate)
);
-- Zadanie 2 --
ALTER TABLE UCZNIOWIE
  ADD(PESEL NUMBER(11) NOT NULL);
-- Zadanie 3 --
ALTER TABLE UCZNIOWIE
  DROP COLUMN data_ur
-- Zadanie 4 --
ALTER TABLE NAUCZYCIELE
	RENAME COLUMN IMIE TO Imie_nauczyciela
-- Zadanie 5 --
ALTER TABLE OCENY
  ADD CONSTRAINT OGR CHECK( OCENA >= 2 AND OCENA <= 6)
-- Zadanie 6 --
INSERT INTO PRZEDMIOTY VALUES(4, 'HISTORIA')
-- Zadanie 7 -- NIEPEWNE
CREATE VIEW WIDOK
	AS SELECT UCZNIOWIE.NAZWISKO, UCZNIOWIE.IMIE, PRZEDMIOTY.NAZWA, NAUCZYCIELE.NAZWISKO, NAUCZYCIELE.IMIE, AVG(OCENY.OCENA)
		FROM UCZNIOWIE, PRZEDMIOTY, NAUCZYCIELE, OCENY
		WHERE UCZNIOWIE.ID_UCZNIA = OCENY.ID_UCZNIA
			AND OCENY.ID_NAUCZYCIELA = NAUCZYCIELE.ID_NAUCZYCIELA
			AND NAUCZYCIELE.ID_PRZEDMIOTU = PRZEDMIOTY.ID_PRZEDMIOTU
			GROUP BY PRZEDMIOT
-- Zadanie 8 --
COMMENT ON COLUMN KLASY.NAZWA IS 'KOMENTARZ...'
-- Zadanie 9 --
SELECT * FROM ALL_CONSTRAINTS
	WHERE TABLE_NAME = 'OCENY'
-- Zadanie 10 --
.........
-- Zadanie 11 --
Omów model ERD
-diagram związków encji, rodzaj graficznego przedstawienia związków pomiędzy encjami używany do:
	- przedstawienia modelu danych
	- analizy zależności fun. w bazie danych
	- wykrywanie i usuwanie błedów z nadmiarowością danych
-Podstawowe elementy modelu ERD to : encja, związek, atrybut
Dwie cechy związków:
	* opcjonalność
		------------ opcjonalność związku
		____________ wymóg związku
	* krotność
		1:1
		1:N
		M:N
-- Zadanie 12 --
Omów model relacyjny bazy danych
	- model ten oparty jest na jednej strukturze zwanej relacją.
	Relacje można uważać za pewną abstrakcję pojęcia tabeli(wiersz, kolumna)
		*każda kolumna zawiera elementy tego samego typu,
		 zawiera swoją nazwę i jest przez nią identyfikowana
		* wiersz tabeli (krotka) identyfikujemy za pomocą wartości elementu (atrybutów)
Dobre zaprojektowanie bazy danych umożliwia łatwe jej zarządzanie
Wyszukiwanie odbywa się za pomocą zapytań (język SQL)s

